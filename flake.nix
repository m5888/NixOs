{
  description = "System Config";
    
  inputs = {
    nixpkgs.url = "nixpkgs/nixos-21.11";
    
    home-manager = {
      url = "github:nix-community/home-manager/release-21.11";
      inputs.nixpkgs.follows = "nixpkgs";
    }; 
  };


  outputs = { nixpkgs, home-manager, ... }@inputs:
  let
    inherit (nixpkgs) lib;
    

    util = import ./lib {
      inherit system pkgs home-manager lib; overlays = (pkgs.overlays);
    };

    inherit (util) user;
    inherit (util) host;

    pkgs = import nixpkgs {
      inherit system;
      config.allowUnfree = true;
      overlays = [];
    };

    system = "x86_64-linux";

    pcConfig = {
      core.enable = true;
      boot = "systemd-boot";
      audio = "pipewire";
      xorg.enable = true;
      xorg.drivers = "nvidia";
      #bspwm.enable = true;
      steam = true;
      gnome.enable = true;
    };
  in {
    homeManagerConfigurations = {
      felix = user.mkHMUser {
        username = "felix";
	userConfig = {
	  git.enable = true;
          applications.enable = true;
          mpd.enable = true;
          doom.enable = true;
          rofi.enable = true;
          #gtk-theme.theme = "Arc";
          #bspwm.enable = true;
          #polybar.enable = true;
          gnome.enable = true;
          alacritty.enable = true;
          ncmpcpp.enable = true;
	};
      };
    };

    nixosConfigurations = {
      nixospc = host.mkHost {
        name = "nixospc";
        NICs = [ "enp34s0" ]; #"wlo1" Doesn't work, Don't need it anyway.
        wifi = true;
        kernelPackage = pkgs.linuxPackages;
	    initrdMods = [ "nvme" "xhci_pci" "ahci" "usbhid" "sd_mod" ];
	    kernelMods = [ "kvm-amd" ];
	    kernelParams = [];
	    systemConfig = pcConfig;
	    users = [{
	      name = "felix";
	      groups = [ "wheel" "networkmanager" ];
	      uid = 1000;
	      shell = pkgs.fish;
	    }];
	    cpuCores = 6;
      };
    };
  };
} 
