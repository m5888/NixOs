{ pkgs, config, lib, ... }:
with lib;

let 
  cfg = config.felix.core;
in 
{
  options.felix.core = {
    enable = mkOption {
      description = "Enable core options";
      type = types.bool;
      default = true;
    };
  };
  
  config = mkIf (cfg.enable) {
    #Makes flakes work
    nix =  {
      package = pkgs.nixFlakes;
      extraOptions = "experimental-features = nix-command flakes";
    };
    
    time.timeZone = "Europe/Berlin";
    #i18n.supportedLocales = [
    #  "en_US.UTF-8/UTF-8"
    #  "de_DE.UTF-8/UTF-8"
    #];
    i18n.defaultLocale = "en_US.UTF-8";
    #i18n.extraLocaleSettings = {
    #  LANG= "en_US.UTF-8";
    #};
    console = {
     font = "Lat2-Terminus16";
     keyMap = "de";
    };

    powerManagement.cpuFreqGovernor = lib.mkDefault "performance";

    programs.dconf.enable = true;

    environment.shells = [ pkgs.zsh pkgs.bash ];

    fonts.fonts = with pkgs; [
      emacs-all-the-icons-fonts
      material-icons
    ];

    environment.systemPackages = with pkgs; [
    #Shells
    fish
    vim
    
    #Web-Stuff
    wget
    curl
    
    #Misc
    htop
    git
    git-crypt
    alacritty
    unzip
    neofetch
   ];
  };
}
