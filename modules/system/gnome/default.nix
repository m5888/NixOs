{ pkgs, config, lib, ... }:
with lib;

let
  cfg = config.felix.gnome;
in {
  options.felix.gnome = {
    enable = mkOption {
      description = "Enable gnome";
      type = types.bool;
      default = false;
    };

  };

  config = mkIf (cfg.enable) {
    environment.systemPackages = with pkgs; [ gnome-menus ];
    services = {
      xserver.displayManager.gdm.enable = true;
      xserver.desktopManager.gnome.enable = true;
      dbus.packages = [ pkgs.gnome.dconf ];
      udev.packages = [ pkgs.gnome.gnome-settings-daemon ];
      gnome.chrome-gnome-shell.enable = true;
    };
  };
}
