{ pkgs, config, lib, ... }:
with lib;

let 
  cfg = config.felix.xorg;
  #chili = "${(pkgs.fetchFromGitHub {
  #  owner = "MarianArlt";
  #  repo = "sddm-chili";
  #  rev = "6516d50176c3b34df29003726ef9708813d06271";
  #  sha256 = "wxWsdRGC59YzDcSopDRzxg8TfjjmA3LHrdWjepTuzgw=";
  #  })}";
in
{
  options.felix.xorg = {
    enable = mkOption {
      description = "Enables X11";
      default = false;
      type = types.bool;
    };
    
    drivers = mkOption {
      description = "Configures Video Drivers.";
      type = types.str;
    };
  };

  config = mkIf ( cfg.enable ) {
    services.xserver = {
      videoDrivers = [ cfg.drivers ];
      enable = true;
      layout = "de";
      screenSection = "Option \"metamodes\" \"2560x1440_144 +0+0 {AllowGSYNCCompatible=On}\" \n";
#      desktopManager = {
#        xterm.enable = false;
#        xfce.enable = true;
#     };
   #  displayManager.sddm = {
   #    enable = true;
   #    theme = chili;
   #  };
      #displayManager.lightdm = {
      #  enable = true;
        #background = ;
      #  greeters.tiny = {
#          enable = true;
      #  };
      #};
      #displayManager.defaultSession = "xfce";
   };
   #Because of driver bug
   hardware.nvidia.package = config.boot.kernelPackages.nvidiaPackages.legacy_470;
  };
}
