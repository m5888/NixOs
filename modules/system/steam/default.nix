{ pkgs, config, lib, ... }:
with lib;

let
  cfg = config.felix.steam;
in {
  options.felix.steam = mkOption{
      description = "Steam system wide";
      type = types.bool;
      default = false;
  };

  config = mkIf (cfg) {
    programs.steam.enable = true;
  };

}
