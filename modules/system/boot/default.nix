{ pkgs, config, lib, ... }:
with lib;

let 
  cfg = config.felix.boot;
in
{
  options.felix.boot = mkOption {
    description = "Bootloader standard is systemd boot";
    default = null;
    type = types.enum [ "systemd-boot" ];
  
  };

  config = mkIf ( cfg == "systemd-boot") {
    boot.loader = {
      systemd-boot.enable = true;
      efi.canTouchEfiVariables = true;
    };

    fileSystems."/" = { 
      device = "/dev/disk/by-label/nixos";
      fsType = "ext4";
    };

    fileSystems."/boot" = { 
      device = "/dev/disk/by-label/boot";
      fsType = "vfat";
    };

    swapDevices = [ 
      { device = "/dev/disk/by-label/swap"; }
    ];

    fileSystems."/home/felix/hdd" = { 
      device = "/dev/disk/by-label/Data";
      fsType = "ext4";
    };
  };
}
