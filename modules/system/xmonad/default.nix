{ pkgs, config, lib, ... }:
with lib;

let 
  cfg = config.felix.xmonad;
in 
{
  options.felix.xmonad = {
    enable = mkOption {
      description = "Enable xmonad systemwide";
      type = types.bool;
      default = false;
    };
  };
  
  config = mkIf (cfg.enable) {
    services = {
      gnome.gnome-keyring.enable = true;
      upower.enable = true;

      dbus = {
        enable = true;
        packages = [ pkgs.gnome.dconf ];
      };

      xserver = {
        windowManager.xmonad = {
          enable = true;
          enableContribAndExtras = true;
        };
      };
    };

  hardware.bluetooth.enable = true;
  services.blueman.enable = true;
  };
}
