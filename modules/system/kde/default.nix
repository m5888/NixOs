{ pkgs, config, lib, ... }:
with lib;

let
  cfg = config.felix.kde;
in {
  options.felix.kde = {
    enable = mkOption {
      description = "Enable KDE";
      type = types.bool;
      default = false;
    };

  };

  config = mkIf (cfg.enable) {
    services.xserver.desktopManager.kde5 = true;
    services.xserver.displayManager.sddm.enable = true;
  };

}
