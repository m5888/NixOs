{ pkgs, config, lib, ... }:
with lib;

let 
  cfg = config.felix.audio;
in
{
  options.felix.audio = mkOption {
    description = "Configures the audio server. Standard is pipewire.";
    default = "pipewire";
    type = types.enum [ "pipewire" ];
  
  };

  config = mkIf ( cfg == "pipewire") {
    security.rtkit.enable = true;
    hardware.pulseaudio.enable = false;
    services.pipewire = {
      enable = true;
      alsa.enable = true;
      alsa.support32Bit = true;
      pulse.enable = true;
      jack.enable = true;
      config.pipewire = {
        "context.properties" = {
          "default.clock.rate" = 48000;
          "default.clock.allowed-rates" = [44100 48000 88200 96000 192000 768000 ];
        
        };
      };
    };
  };
}
