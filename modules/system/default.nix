{ pkgs, config, lib, ... }:
{
  imports = [
    ./core
    ./boot
    ./audio
    ./xorg
    #./xmonad
    ./steam
    ./bspwm
    ./kde
    ./gnome
  ];
}
