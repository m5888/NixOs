{ pkgs, config, lib, ... }:
with lib;

let 
  cfg = config.felix.bspwm;
in 
{
  options.felix.bspwm = {
    enable = mkOption {
      description = "Enable bspwm systemwide";
      type = types.bool;
      default = false;
    };
  };
  
  config = mkIf (cfg.enable) {
    services = {
      gnome.gnome-keyring.enable = true;
      upower.enable = true;

      dbus = {
        enable = true;
        packages = [ pkgs.gnome.dconf ];
      };

      xserver = {
        windowManager.bspwm = {
          enable = true;
#          configFile = ./bspwmrc;
#          sxhkd.configFile = ./sxhkdrc;
        };
        displayManager.defaultSession = "none+bspwm";
      };
    };

  hardware.bluetooth.enable = true;
  services.blueman.enable = true;
  };
}
