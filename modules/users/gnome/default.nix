{ pkgs, config, lib, ... }:
with lib;

let
  cfg = config.felix.gnome;
in {
  options.felix.gnome = {
    enable = mkOption {
      description = "";
      type = types.bool;
      default = false;
    };

  };

  config = mkIf (cfg.enable) {
    programs.firefox.enableGnomeExtensions = true;
    home.packages = with pkgs; [
      arc-theme
      gnome.eog
      gnome.evince

      gnome.gnome-tweak-tool
      gnome.gnome-shell-extensions

      #extensions
      gnomeExtensions.gsconnect
      gnomeExtensions.dash-to-panel
      #gnomeExtensions.arcmenu
      #gnome-menus
      gnomeExtensions.appindicator
      gnomeExtensions.blur-my-shell
    ];
  };

}
