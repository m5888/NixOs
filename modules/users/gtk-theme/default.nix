{ pkgs, config, lib, ... }:
with lib;

let
  cfg = config.felix.gtk-theme;
  gtkDir = ~/.themes;
in {
  options.felix.gtk-theme = {
    theme = mkOption {
      description = "Sets GTK theme";
      type = types.str;
      default = "adwaita";
    };

  };

  config = mkIf (cfg.theme == "Dracula") {
    gtk = {
      enable = true;
      theme = {
        name = "Dracula";
        package = pkgs.dracula-theme;
      };
    };
    #home.file = {
    #  "${gtkDir}".folder = ./Dracula;
    #};
    home.packages = with pkgs; [ papirus-icon-theme ];
  };


}
