{ pkgs, config, lib, ... }:
with lib;

let
  cfg = config.felix.ncmpcpp;
in {
  options.felix.ncmpcpp = {
    enable = mkOption {
      description = "Enable Ncmpcpp with ueberzug";
      type = types.bool;
      default = false;
    };

  };

  config = mkIf (cfg.enable) {
    home.packages = with pkgs; [
      ncmpcpp
      ueberzug
      mpc_cli
      flac
      ffmpeg
    ];
    home.file.".ncmpcpp" = {
      recursive = true;
      source = ./config;
    };
    home.sessionPath = [ "$HOME/.ncmpcpp/ncmpcpp-ueberzug" ];
  };

}
