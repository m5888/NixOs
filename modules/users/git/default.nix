{ pkgs, config, lib, ... }:
with lib;

let
  cfg = config.felix.git;
in {
  options.felix.git = {
    enable = mkOption {
      description = "Enable git";
      type = types.bool;
      default = false;
    };

    userName = mkOption {
      description = "Name for git";
      type = types.str;
      default = "Felix Plamper";
    };

    userEmail = mkOption {
      description = "Git Email";
      type = types.str;
      default = "example@example.com";
    };
  };

  config = mkIf (cfg.enable) {
    programs.git = {
      enable = true;
      userName = cfg.userName;
      userEmail = cfg.userEmail;
    };
  };

}
