{ pkgs, lib, config, ... }:
{
  imports = [
    ./git
    ./applications
    ./mpd
    ./doom
    ./rofi
    ./gtk-theme
    ./bspwm
    ./polybar
    ./alacritty
    ./ncmpcpp
    ./gnome
  ];
}
