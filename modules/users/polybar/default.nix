{ pkgs, config, lib, ... }:
with lib;

let
  cfg = config.felix.polybar;
  mybar = pkgs.polybar.override {
    mpdSupport = true;
    #pipewireSupport = true;
    githubSupport = true;
    pulseSupport = true;
    i3Support = true;
  };
in {
  options.felix.polybar = {
    enable = mkOption {
      description = "Enable Polybar";
      type = types.bool;
      default = false;
    };

  };

  config = mkIf (cfg.enable) {
    services.polybar = {
    #  enable = true;
    #  package = mybar;
      #script = "polybar main -c ~/.config/polybar/config.ini &";
      #config = ./config.ini;
    };
    home.packages = with pkgs; [ mybar lxsession ];
    home.file.".config/polybar" = {
      recursive = true;
      source = ./polybar;
    };
    home.file."polybar-collection/themes/dracula" = {
      recursive = true;
      source = ./polybar;
    };
  };

}
