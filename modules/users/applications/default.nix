{ pkgs, lib, config, ... }:
with lib;
let
  cfg = config.felix.applications;
in 
{
  options.felix.applications = {
    enable = mkOption {
      description = "Enable standard user apps";
      type = types.bool;
      default = false;
    };
  };
  config = mkIf (cfg.enable) {
    
    programs.firefox ={
      enable = true;
    };
    fonts.fontconfig.enable = true;
    #Home packages 
    home.packages = with pkgs; [
      killall
      lxappearance
      cinnamon.nemo
      gnome.nautilus
      discord
      nnn
      ncmpcpp
      nerdfonts
      material-icons
      gimp
      escrotum

      bitwarden
      libreoffice
      cantata

      multimc
      jdk
      lutris
      openssl

      #Devolpment Stuff
      gcc
      gnumake
#Wine stuff
      wine
      dotnet-netcore
      mono
    ];
  };

}
