{ pkgs, config, lib, ... }:
with lib;

let
  cfg = config.felix.rofi;
in {
  options.felix.rofi = {
    enable = mkOption {
      description = "";
      type = types.bool;
      default = false;
    };

  };

  config = mkIf (cfg.enable) {
    programs.rofi = {
      enable = true;
      terminal = "\${pkgs.alacritty}/bin/alacritty";
      theme = ./dracula.rasi;
    };
  };

}
