{ pkgs, config, lib, ... }:
with lib;

let
  cfg = config.felix.xmonad;
in {
  options.felix.xmonad = {
    enable = mkOption {
      description = "Enables my Xmonad config";
      type = types.bool;
      default = false;
    };

  };

  config = mkIf (cfg.enable) {
   windowManager.xmonad = {
      enable = true;
      enableContribAndExtras = true;
      extraPackages = hp: [
        hp.dbus
        hp.monad-logger
        hp.xmonad-contrib
      ];
      config = ./xmonad.hs;
    };
  };

}
