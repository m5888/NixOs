{ pkgs, config, lib, ... }:
with lib;
let
  cfg = config.felix.doom;
  
  doom-emacs = pkgs.callPackage (builtins.fetchTarball {
    url = https://github.com/vlaci/nix-doom-emacs/archive/master.tar.gz;
  }) 
  {
  doomPrivateDir = ./doom.d;  # Directory containing your config.el init.el
                                # and packages.el files
  };
  
in {
  options.felix.doom = {
    enable = mkOption {
      description = "Enable Doom";
      type = types.bool;
      default = false;
    };
  };
  config = mkIf (cfg.enable) {
  home.packages = [ doom-emacs ];
  home.file.".emacs.d/init.el".text = ''
      (load "default.el")
  '';
  services.emacs = {
    enable = true;
    package = doom-emacs;
  };
  };
}
