{ pkgs, config, lib, ... }:
with lib;

let
  cfg = config.felix.alacritty;
in {
  options.felix.alacritty = {
    enable = mkOption {
      description = "Set dracula theme";
      type = types.bool;
      default = false;
    };

  };

  config = mkIf (cfg.enable) {
    home.file.".config/alacritty/alacritty.yml".source = ./dracula.yml;

  };
}
