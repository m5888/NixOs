{ pkgs, lib, config, ... }:
with lib;
let
  cfg = config.felix.mpd;
in 
{
  options.felix.mpd = {
    enable = mkOption {
      description = "Enable mpd";
      type = types.bool;
      default = false;
    };
  };
  config = mkIf (cfg.enable) {
    services.mpd = {
      enable = true;
      musicDirectory = "~/Music";
      extraConfig = '' 
        audio_output {  
          type	"pipewire" #
          name  "Pipewire"  #
          dsd         "yes"  #
        }
     '';    
   };
   
  };

}